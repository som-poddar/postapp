require 'json'
require 'curb'

SERVICE_URL = 'http://localhost:3000'

begin
  # put a comment & a sub-comment
  c = Curl::Easy.http_post(
        "#{SERVICE_URL}/posts/1/comments",
        Curl::PostField.content('comment[content]', 'This should be a comment at root level.'),
        Curl::PostField.content('comment[user_id]', 1),
        Curl::PostField.content('comment[post_id]', 1)
      )

  c = Curl::Easy.http_post(
      "#{SERVICE_URL}/posts/1/comments",
       Curl::PostField.content('comment[content]', 'This should be my nested'),
       Curl::PostField.content('comment[user_id]', 1),
       Curl::PostField.content('comment[post_id]', 1),
       Curl::PostField.content('comment[parent_id]', 1)
      )

  http = Curl.get("#{SERVICE_URL}/posts") do|http|
    http.headers['Accept'] = 'application/json'
  end

  json_response = JSON.parse(http.body_str)
  if json_response['status'] == 'success' &&
     json_response['data'] &&
     json_response['data'].size > 0

    # print to console
    json_response['data'].each do |post|
      puts
      puts "title:#{post['title']}"
      puts "author:#{post['author_name']}(#{post['author_city']})"
      puts "#{post['content']}"
      puts "comments:#{post['comments']}"
      puts
    end
  else
    puts 'did not find valid response'
  end
rescue => e
  puts "something went wrong. #{e.message}:"
end
