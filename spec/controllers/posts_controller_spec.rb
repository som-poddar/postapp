require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  it 'is available before an action' do
    expect(controller).to be_an_instance_of(PostsController)
  end

  describe 'GET #index' do
    it 'index route should work successfully' do
      get :index, format: 'json'
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response.content_type).to eql('application/json')
      json_response = JSON.parse(response.body)
      expect(json_response['data'].size).to eql(2)
      expect(json_response['status']).to eql('success')
    end
  end

  describe 'POST #create' do
    it 'it should be able to create a new post' do
      user = User.find(1)
      @post = Post.create(
        title: 'Peter Griffin',
        content: 'I like Family Guy, period.',
        user: user)
      expect(@post).to be_an_instance_of Post
    end
  end

  it 'should be able to view an individual post' do
    get :show, id: 1, format: 'json'
    expect(response).to be_success
    expect(response).to have_http_status(200)
    expect(response.content_type).to eql('application/json')
    json_response = JSON.parse(response.body)
    expect(json_response['data']['id']).to eql(1)
  end

  it 'should be able to update post' do
    @post = Post.find(1)
    put :update, id: @post.id, post: @post.attributes = { title: 'new title', content: 'new content' }, format: 'json'
    @post.reload
    expect(response).to be_success
    expect(response).to have_http_status(200)
    json_response = JSON.parse(response.body)
    expect(json_response['status']).to eql('updated')
  end

  it 'should be able to delete a post' do
    @post = Post.find(1)
    delete :destroy, id: @post.id, format: 'json'
    expect(response).to be_success
    expect(response).to have_http_status(200)
    json_response = JSON.parse(response.body)
    expect(json_response['status']).to eql('deleted')
  end
end
