users = User.create(
  [
    { name: 'Som Poddar', email: 'sp@email.com', city: 'SF' },
    { name: 'Some other Poddar', email: 'sp2@email.com', city: 'NY' }
  ]
)

posts = Post.create(
  [
    { title: 'Very First Post', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', user: users[0] },
    { title: 'Second Post', content: 'Repeat, Lorem Ipsum is simply dummy text of the printing and typesetting industry', user: users[1] }
  ]
)

comments = Comment.create(
  [
    { content: 'Some opinion from me', post: posts[0], user: users[0] },
    { content: 'Another opinion from me', post: posts[0], user: users[1] }
  ]
)
