class UsersController < ApplicationController
  respond_to :json
  before_action :set_user, only: [:show, :update, :destroy]

  def index
    @users = User.all
    render json: { data: @users, status: 'success' }
  end

  def show
    render json: { data: @user, status: 'success' }
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: { status: 'created', id: @user.id }
    else
      render json: { status: 'error' }
    end
  end

  def update
    if @user.update(user_params)
      render json: { status: 'updated', data: @user }
    else
      render json: { status: 'error' }
    end
  end

  def destroy
    @user.destroy
    render json: { status: 'deleted' }
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :email, :city)
  end
end
