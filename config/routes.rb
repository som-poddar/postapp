Rails.application.routes.draw do
  resources :posts, except: [:edit, :new] do
    resources :comments, only: [:create, :index]
    resources :images, only: [:create, :destroy]
  end
  get 'list' => 'posts#index'
  resources :users
end
