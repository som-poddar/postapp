class ImagesController < ApplicationController
  respond_to :json
  before_action :set_image, only: [:destroy]

  def create
    @image = Image.new(image_params)
    if @image.save
      render json: { status: 'created', id: @image.id }
    else
      render json: { status: 'error' }
    end
  end

  def destroy
    @image.destroy
    render json: { status: 'deleted' }
  end

  private

  def set_image
    @image = Image.find(params[:id])
  end

  def image_params
    params.require(:image).permit(:url, :alt_text, :post_id)
  end
end
