class PostsController < ApplicationController
  respond_to :json
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    @posts = Post.all.order('created_at DESC')
    final_response = @posts.collect do |x|
      { 'id' => x.id,
        'title' => x.title,
        'author_name' => x.user.name,
        'author_city' => x.user.city,
        'content' => x.content,
        'images' => x.images.collect(&:url),
        'comments' => x.comments
      }
    end
    render json: { data: final_response, status: 'success' }
  end

  def show
    if @post
      render json: { status: 'created', data: @post }
    else
      render json: { status: 'error' }
    end
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      render json: { status: 'created', id: @post.id }
    else
      render json: { status: 'error' }
    end
  end

  def update
    if @post.update(post_params)
      render json: { status: 'updated', data: @post }
    else
      render json: { status: 'error' }
    end
  end

  def destroy
    @post.destroy
    render json: { status: 'deleted' }
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :content, :user_id)
  end
end
