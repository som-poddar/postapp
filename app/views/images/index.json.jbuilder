json.array!(@images) do |image|
  json.extract! image, :id, :url, :alt_text, :post_id
  json.url image_url(image, format: :json)
end
