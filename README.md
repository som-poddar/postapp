## PostApp API

#### How to Get Started

```rb
bundle exec rake db:reset
bundle exec rspec
bundle rails s
```

#### Specification

https://gist.github.com/yardclub-dev/e8053f58a611287f5a95


#### Assumptions
- The App only returns json response
- The view files are not deleted, but they can be removed as 
they are not needed in the current implementation

#### Future Works
- Implement paginations for posts
- Extend the Rspec tests to use mock
- Add unicorn for running the app outside dev environment
