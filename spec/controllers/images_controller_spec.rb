require 'rails_helper'

RSpec.describe ImagesController, type: :controller do
  it 'is available before an action' do
    expect(controller).to be_an_instance_of(ImagesController)
  end

  describe 'Image #create' do
    it 'should be able to create a new image' do
      @post = Post.find(1)
      post :create, post_id: @post, image: { url: 'imgur.com/abc', alt_text: 'some description', post_id: @post }, format: 'json'
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response.content_type).to eql('application/json')
    end
  end
end
