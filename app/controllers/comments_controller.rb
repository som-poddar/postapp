class CommentsController < ApplicationController
  respond_to :json
  before_action :set_comment, only: [:destroy]

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      render json: { status: 'created', id: @comment.id }
    else
      render json: { status: 'error' }
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content, :user_id, :post_id, :parent_id)
  end
end
