require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  it 'is available before an action' do
    expect(controller).to be_an_instance_of(CommentsController)
  end
end
